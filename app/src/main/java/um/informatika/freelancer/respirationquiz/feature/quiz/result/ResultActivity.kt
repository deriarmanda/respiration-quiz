package um.informatika.freelancer.respirationquiz.feature.quiz.result

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_result.*
import nl.dionsegijn.konfetti.models.Shape
import nl.dionsegijn.konfetti.models.Size
import um.informatika.freelancer.respirationquiz.R
import um.informatika.freelancer.respirationquiz.feature.quiz.DetailQuizActivity

class ResultActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_SCORE = "extra_score"
        private const val EXTRA_DETAIL = "extra_detail"

        fun getIntent(context: Context, score: Int, detail: String): Intent {
            val intent = Intent(context, ResultActivity::class.java)
            intent.putExtra(EXTRA_SCORE, score)
            intent.putExtra(EXTRA_DETAIL, detail)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val score = intent.getIntExtra(EXTRA_SCORE, 0)
        text_score.text = score.toString()

        val detail = intent.getStringExtra(EXTRA_DETAIL)
            ?: getString(R.string.quiz_msg_score_detail, 0, 0, 0)
        text_desc.text = detail

        fab_exit.setOnClickListener { finish() }
        fab_retry.setOnClickListener {
            startActivity(DetailQuizActivity.getIntent(this))
            finish()
        }

        konfetti.build()
            .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
            .setDirection(0.0, 359.0)
            .setSpeed(1f, 5f)
            .setFadeOutEnabled(true)
            .setTimeToLive(2000L)
            .addShapes(Shape.RECT, Shape.CIRCLE)
            .addSizes(Size(12, 5f))
            .setPosition(-50f, konfetti.width + 250f, -50f, -50f)
            .streamFor(100, 500000L)

        text_desc.isSelected = true
    }
}
