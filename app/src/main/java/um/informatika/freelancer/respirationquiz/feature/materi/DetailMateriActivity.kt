package um.informatika.freelancer.respirationquiz.feature.materi

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detail_materi.*
import um.informatika.freelancer.respirationquiz.R

class DetailMateriActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, DetailMateriActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_materi)

        pdf_view.fromAsset("materi.pdf")
            .defaultPage(0)
            .enableSwipe(true)
            .swipeHorizontal(false)
            .spacing(0)
            .load()
    }
}
