package um.informatika.freelancer.respirationquiz.feature.quiz

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detail_quiz.*
import um.informatika.freelancer.respirationquiz.R
import um.informatika.freelancer.respirationquiz.feature.quiz.result.ResultActivity

class DetailQuizActivity : AppCompatActivity() {

    private val quizList = arrayListOf<Quiz>()
    private var number = -1

    companion object {
        fun getIntent(context: Context) = Intent(context, DetailQuizActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_quiz)

        with(view_flipper) {
            isAutoStart = false
            setFlipInterval(500)
        }
        fab_next.setOnClickListener { showNextQuiz() }
        fab_prev.setOnClickListener { showPrevQuiz() }
        fab_finish_1.setOnClickListener { countAndShowScore() }
        fab_finish_2.setOnClickListener { countAndShowScore() }

        prepareQuiz()
        showNextQuiz()
    }

    private fun prepareQuiz() {
        val questions = resources.getStringArray(R.array.quiz_questions)
        val keys = resources.getStringArray(R.array.quiz_keys)
        val answerAs = resources.getStringArray(R.array.quiz_a_answers)
        val answerBs = resources.getStringArray(R.array.quiz_b_answers)
        val answerCs = resources.getStringArray(R.array.quiz_c_answers)
        val answerDs = resources.getStringArray(R.array.quiz_d_answers)
        val answerEs = resources.getStringArray(R.array.quiz_e_answers)

        for (i in 0 until questions.size) quizList.add(
            if (i == 1) Quiz(
                questions[i],
                keys[i],
                answerAs[i],
                answerBs[i],
                answerCs[i],
                answerDs[i],
                answerEs[i],
                questionDrawableRes = R.drawable.img_soal_2
            )
            else if (i == 7) Quiz(
                questions[i],
                keys[i],
                answerAs[i],
                answerBs[i],
                answerCs[i],
                answerDs[i],
                answerEs[i],
                questionDrawableRes = R.drawable.img_soal_8
            )
            else if (i == 19) Quiz(
                questions[i],
                keys[i],
                answerAs[i],
                answerBs[i],
                answerCs[i],
                answerDs[i],
                answerEs[i],
                questionDrawableRes = R.drawable.img_soal_20
            )
            else Quiz(questions[i], keys[i], answerAs[i], answerBs[i], answerCs[i], answerDs[i], answerEs[i])
        )

        card_answer_a_1.setOnClickListener {
            setAllChoiceBoxToGreen(1)
            card_answer_a_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.accent, theme))
            quizList[number].answer = "A"
            showNextQuiz()
        }
        card_answer_b_1.setOnClickListener {
            setAllChoiceBoxToGreen(1)
            card_answer_b_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.accent, theme))
            quizList[number].answer = "B"
            showNextQuiz()
        }
        card_answer_c_1.setOnClickListener {
            setAllChoiceBoxToGreen(1)
            card_answer_c_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.accent, theme))
            quizList[number].answer = "C"
            showNextQuiz()
        }
        card_answer_d_1.setOnClickListener {
            setAllChoiceBoxToGreen(1)
            card_answer_d_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.accent, theme))
            quizList[number].answer = "D"
            showNextQuiz()
        }
        card_answer_e_1.setOnClickListener {
            setAllChoiceBoxToGreen(1)
            card_answer_e_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.accent, theme))
            quizList[number].answer = "E"
            showNextQuiz()
        }

        card_answer_a_2.setOnClickListener {
            setAllChoiceBoxToGreen(2)
            card_answer_a_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.accent, theme))
            quizList[number].answer = "A"
            showNextQuiz()
        }
        card_answer_b_2.setOnClickListener {
            setAllChoiceBoxToGreen(2)
            card_answer_b_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.accent, theme))
            quizList[number].answer = "B"
            showNextQuiz()
        }
        card_answer_c_2.setOnClickListener {
            setAllChoiceBoxToGreen(2)
            card_answer_c_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.accent, theme))
            quizList[number].answer = "C"
            showNextQuiz()
        }
        card_answer_d_2.setOnClickListener {
            setAllChoiceBoxToGreen(2)
            card_answer_d_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.accent, theme))
            quizList[number].answer = "D"
            showNextQuiz()
        }
        card_answer_e_2.setOnClickListener {
            setAllChoiceBoxToGreen(2)
            card_answer_e_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.accent, theme))
            quizList[number].answer = "E"
            showNextQuiz()
        }
    }

    private fun showNextQuiz() {
        if (number + 1 >= quizList.size) countAndShowScore()
        else {
            val quiz = quizList[++number]
            text_number.text = String.format("%d", number + 1)
            if (number == quizList.size - 1) {
                fab_finish_1.show()
                fab_finish_2.show()
            } else {
                fab_finish_1.hide()
                fab_finish_2.hide()
            }

            if (number % 2 == 0) {
                // Index Genap
                text_question_2.text = quiz.question
                text_question_2.setCompoundDrawablesWithIntrinsicBounds(0, quiz.questionDrawableRes, 0, 0)
                text_answer_a_2.text = quiz.answerA
                text_answer_b_2.text = quiz.answerB
                text_answer_c_2.text = quiz.answerC
                text_answer_d_2.text = quiz.answerD
                text_answer_e_2.text = quiz.answerE

                setAllChoiceBoxToGreen(2)
                when (quiz.answer) {
                    "A" -> card_answer_a_2.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "B" -> card_answer_b_2.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "C" -> card_answer_c_2.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "D" -> card_answer_d_2.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "E" -> card_answer_e_2.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                }
            } else {
                // Index Ganjil
                text_question_1.text = quiz.question
                text_question_1.setCompoundDrawablesWithIntrinsicBounds(0, quiz.questionDrawableRes, 0, 0)
                text_answer_a_1.text = quiz.answerA
                text_answer_b_1.text = quiz.answerB
                text_answer_c_1.text = quiz.answerC
                text_answer_d_1.text = quiz.answerD
                text_answer_e_1.text = quiz.answerE

                setAllChoiceBoxToGreen(1)
                when (quiz.answer) {
                    "A" -> card_answer_a_1.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "B" -> card_answer_b_1.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "C" -> card_answer_c_1.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "D" -> card_answer_d_1.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "E" -> card_answer_e_1.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                }
            }

            view_flipper.setInAnimation(this, R.anim.in_from_bottom)
            view_flipper.setOutAnimation(this, R.anim.out_to_top)
            view_flipper.showNext()
        }
    }

    private fun showPrevQuiz() {
        if (number - 1 >= 0) {
            val quiz = quizList[--number]
            text_number.text = String.format("%d", number + 1)
            if (number == quizList.size - 1) {
                fab_finish_1.show()
                fab_finish_2.show()
            } else {
                fab_finish_1.hide()
                fab_finish_2.hide()
            }

            if (number % 2 == 0) {
                // Index Genap
                text_question_2.text = quiz.question
                text_question_2.setCompoundDrawablesWithIntrinsicBounds(0, quiz.questionDrawableRes, 0, 0)
                text_answer_a_2.text = quiz.answerA
                text_answer_b_2.text = quiz.answerB
                text_answer_c_2.text = quiz.answerC
                text_answer_d_2.text = quiz.answerD
                text_answer_e_2.text = quiz.answerE

                setAllChoiceBoxToGreen(2)
                when (quiz.answer) {
                    "A" -> card_answer_a_2.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "B" -> card_answer_b_2.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "C" -> card_answer_c_2.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "D" -> card_answer_d_2.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "E" -> card_answer_e_2.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                }
            } else {
                // Index Ganjil
                text_question_1.text = quiz.question
                text_question_1.setCompoundDrawablesWithIntrinsicBounds(0, quiz.questionDrawableRes, 0, 0)
                text_answer_a_1.text = quiz.answerA
                text_answer_b_1.text = quiz.answerB
                text_answer_c_1.text = quiz.answerC
                text_answer_d_1.text = quiz.answerD
                text_answer_e_1.text = quiz.answerE

                setAllChoiceBoxToGreen(1)
                when (quiz.answer) {
                    "A" -> card_answer_a_1.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "B" -> card_answer_b_1.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "C" -> card_answer_c_1.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "D" -> card_answer_d_1.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                    "E" -> card_answer_e_1.setCardBackgroundColor(
                        ResourcesCompat.getColor(
                            resources,
                            R.color.accent,
                            theme
                        )
                    )
                }
            }

            view_flipper.setInAnimation(this, R.anim.in_from_top)
            view_flipper.setOutAnimation(this, R.anim.out_to_bottom)
            view_flipper.showPrevious()
        }
    }

    private fun countAndShowScore() {
        var right = 0
        var wrong = 0
        var empty = 0

        for (quiz in quizList) {
            when {
                quiz.answer == quiz.key -> right++
                quiz.answer.isBlank() -> empty++
                else -> wrong++
            }
        }
        val score = (right * 100) / quizList.size
        val detail = getString(R.string.quiz_msg_score_detail, right, wrong, empty)

        startActivity(ResultActivity.getIntent(this, score, detail))
        finish()
    }

    private fun setAllChoiceBoxToGreen(type: Int) {
        when (type) {
            1 -> {
                card_answer_a_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_b_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_c_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_d_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_e_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
            }
            2 -> {
                card_answer_a_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_b_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_c_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_d_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_e_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
            }
            else -> {
                card_answer_a_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_b_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_c_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_d_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_e_1.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_a_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_b_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_c_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_d_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
                card_answer_e_2.setCardBackgroundColor(ResourcesCompat.getColor(resources, R.color.primary, theme))
            }
        }
    }

    data class Quiz(
        val question: String,
        val key: String,
        val answerA: String,
        val answerB: String,
        val answerC: String,
        val answerD: String,
        val answerE: String,
        var answer: String = "",
        val questionDrawableRes: Int = R.color.solid_white
    )
}
