package um.informatika.freelancer.respirationquiz.feature.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*
import um.informatika.freelancer.respirationquiz.R
import um.informatika.freelancer.respirationquiz.feature.about.AboutActivity
import um.informatika.freelancer.respirationquiz.feature.materi.DetailMateriActivity
import um.informatika.freelancer.respirationquiz.feature.quiz.DetailQuizActivity

class HomeActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, HomeActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        menu_materi.setOnClickListener { startActivity(DetailMateriActivity.getIntent(this)) }
        menu_quiz.setOnClickListener { startActivity(DetailQuizActivity.getIntent(this)) }
        menu_about.setOnClickListener { startActivity(AboutActivity.getIntent(this)) }
    }
}
